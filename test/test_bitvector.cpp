#include <systemc.h>
#include <iostream>
#include <string>

int sc_main (int ac, char ** av) {

    sc_bv<5> positions = "01101";
    sc_bv<6> mask = "100111";
    sc_bv<5> active = positions & mask;
    sc_bv<1> all = active.and_reduce();
    active.range(3,2) = "00";
    active = active | all;

    return (active.to_string().compare("00001") == 0 ? 0 : -1);
}
