#define SC_INCLUDE_FX
#include <systemc.h>
#include <iostream>
#include <string>

int sc_main (int ac, char ** av) {

    const sc_ufixed<19,3> PI("3.141592654");
    sc_fix oil_temp(20,17);
    sc_fixed_fast<7,1> valve_opening;

    oil_temp += PI;

    return (oil_temp == 3.125 ? 0 : -1);
}
