#include <systemc.h>
#include <iostream>
#include <string>

int sc_main (int ac, char ** av) {

    sc_int<5> small_signed=-3;
    sc_uint<13> large(4000);
    sc_uint<14> large_signed = large + small_signed;
    sc_biguint<320> very_large;
    return (large_signed == 3997 ? 0 : -1);
}
