#include <systemc.h>
#include <iostream>
#include <string>

int sc_main (int ac, char ** av) {

    sc_lv<5> positions = "01xz1";
    sc_lv<6> mask = "10ZX11";
    sc_lv<5> active = positions & mask;
    active.range(0,0) = "z";
    sc_logic reduction = active.or_reduce(); 
    return (reduction == SC_LOGIC_X ? 0 : -1);
}
