#include <systemc.h>
#include <iostream>
#include <string>

int sc_main (int ac, char ** av) {

    bool error = false;

    sc_uint<3> myInt = 6;
    sc_lv<4> a = myInt;
    error |= (a.to_uint() != myInt.to_uint());

    myInt += 2;
    sc_bv<3> b = myInt;
    error |= (b.to_uint() != myInt.to_uint());

    sc_bv<5> c = 25;
    error |= (c.to_uint() != 25);
    error |= (c.to_int() == 25);

    return (error ? -1 : 0);
}
